module Evaluations
    def self.table_name_prefix
        'evaluation_'
    end

    class Engine < ::Rails::Engine
        config.generators do |g|
            g.test_framework      :rspec,        fixture: false
            g.fixture_replacement :factory_girl, dir: 'spec/factories'
            g.assets false
            g.helper false
            g.template_engine     :haml
        end
    end
end
