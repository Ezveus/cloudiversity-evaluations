$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "evaluations/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
    s.name        = "cloudiversity_evaluations"
    s.version     = Evaluations::VERSION
    s.authors     = ["Matthieu \"Ezveus\" Ciappara"]
    s.email       = ["ciappam@gmail.com"]
    s.homepage    = "http://cloudiversity.eu"
    s.summary     = "Evaluations plugin for Cloudiversity"
    s.description = <<EOF
Evaluations plugin for Cloudiversity.
EOF

    s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

    s.add_dependency "rails", "~> 4.0.4"
    s.add_dependency "haml-rails"

    s.add_development_dependency "sqlite3"
    s.add_development_dependency "rspec-rails"
    s.add_development_dependency "factory_girl_rails"

    s.test_files = Dir["spec/**/*"]
end
